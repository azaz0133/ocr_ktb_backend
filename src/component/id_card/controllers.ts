import ID from './model'

const IdControllers = {
    getAll(req,res){
        const {page,perPage} = req.query
        const id_card = ID.paginate({},page,perPage).then(results => {
         res.set('Content-Type', 'application/json');
         if(results !== undefined){
             res.status(200).json({
                 id_card:results
             })
         }
         else {
             res.status(404).json("NOT FOUND")
             
          }
        })
    },
    getById(req,res){
        res.set('Content-Type', 'application/json');
        ID.findById(req.params.id).then(results => {
            res.set('Content-Type', 'application/json');
            res.status(200).json({
                id_card:results
            })
        }).catch(err => {
            res.status(404).json({
                id_card:"NOT FOUND"
            })
        }) 
    },
    create(req,res){
        const {body} = req
        let x = ID.createId(req.params.id).then( x=>{
            res.status(200).json({
                id_card : x
            })
        }).catch(err=>console.log(err))
        
    },
    editById(req,res){
        const {body} = req
        const result = ID.update(req.params.id,body).then(result => {
            res.set('Content-Type', 'application/json');
                if(result !== undefined){
                    res.status(201).json({
                        id_card:result
                    })
                }
                else {
                    res.status(404).json("NOT FOUND")
                    
                }
          })
    },
    deleteById(req,res){
        const result = ID.destroy(req.params.id).then(async results => {
            res.set('Content-Type', 'application/json');
            res.status(201).json({"result":"delete finish"})
          })
    }

}

export default IdControllers