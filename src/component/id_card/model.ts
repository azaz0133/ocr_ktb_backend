import Model from '../model'
import { getConnection } from 'typeorm';
const vision = require('@google-cloud/vision');
const client = new vision.ImageAnnotatorClient();
const Id = {
    ...Model,
    relations:[],
    key:'ID_CARD',
    async createId(id){
        return new Promise((resolve,reject)=>{
          console.log(id)
          client
          .documentTextDetection(`C:/Development/Workroom/SoftwareEngineering/Backend/src/asset/${id}`)
              .then(async res => {
                  let content = res[0].fullTextAnnotation.text
                  let token = content.match(/\S+/g)
                  let sex,firstNameTh,lastNameTh,address,firstNameEn,lastNameEn,birthdate,idcard 
                  console.log(token)
                  token.map((data,index)=>{
                      if(data.includes('Card')){
                        if(isNaN(token[index+5]))
                        idcard = token[index+2]+token[index+3]+token[index+4]+token[index+5]+token[index+6]
                        idcard = token[index+2]+token[index+3]+token[index+4]+token[index+5]
                      }
                      if(data.includes('ชื่อตัวและชื่อสกุล')){
                          firstNameTh = token[index+2]
                          lastNameTh = token[index+3]
                          firstNameEn = token[index+6]
                          lastNameEn = token[index+9]
                      }
                      if(data.includes('ที่อยู่')){
                          address = token[index+1]+token[index+2]+token[index+3]+token[index+4]+token[index+5]
                      }
                      if(data.includes("Birth")){
                          birthdate = `${token[index+1]} ${token[index+2]} ${token[index+3]}`
                       }
                      if(data.includes("Mr.")) 
                        sex = "ชาย"
                      if(data.includes('Miss'))
                        sex= "หญิง"
                  })
                  await getConnection()
                  .createQueryBuilder()
                  .insert()
                  .into(this.collection())
                  .values([{
                    Id_cardnumber:idcard,
                    firstNameTh,
                    lastNameTh,
                    firstNameEn,
                    lastNameEn,
                    birthDate:birthdate,
                    Address:address,
                    sex
                  }])
                  .execute()
               resolve( {
                  firstNameTh,
                  lastNameTh,
                  firstNameEn,
                  lastNameEn,
                  birthdate,
                  address
               })
              })
    })
}
}




export default Id