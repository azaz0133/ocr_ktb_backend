import Bookbank from './model'

const StatementControllers = {
    getAll(req,res){
        const {page,perPage} = req.query
        const bookbank = Bookbank.paginate({},page,perPage).then(results => {
         res.set('Content-Type', 'application/json');
         if(results !== undefined){
             res.status(200).json({
                 bookbank:results
             })
         }
         else {
             res.status(404).json("NOT FOUND")
             
          }
        })
    },
    getById(req,res){
        res.set('Content-Type', 'application/json');
        Bookbank.findById(req.params.id).then(results => {
            res.set('Content-Type', 'application/json');
            res.status(200).json({
                bookbank:results
            })
        }).catch(err => {
            res.status(404).json({
                bookbank:"NOT FOUND"
            })
        }) 
    },
    create(req,res){
        const {body} = req
        let x = Bookbank.createId(req.params.id).then( x=>{
            res.status(200).json({
                bookbank : x
            })
        }).catch(err=>console.log(err))
        
    },
    editById(req,res){
        const {body} = req
        const result = Bookbank.update(req.params.id,body).then(result => {
            res.set('Content-Type', 'application/json');
                if(result !== undefined){
                    res.status(201).json({
                        bookbank:result
                    })
                }
                else {
                    res.status(404).json("NOT FOUND")
                    
                }
          })
    },
    deleteById(req,res){
        const result = Bookbank.destroy(req.params.id).then(async results => {
            res.set('Content-Type', 'application/json');
            res.status(201).json({"result":"delete finish"})
          })
    }

}

export default StatementControllers