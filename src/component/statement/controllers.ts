import Statement from './model'

const StatementControllers = {
    getAll(req,res){
        const {page,perPage} = req.query
        const statement = Statement.paginate({},page,perPage).then(results => {
         res.set('Content-Type', 'application/json');
         if(results !== undefined){
             res.status(200).json({
                 statement:results
             })
         }
         else {
             res.status(404).json("NOT FOUND")
             
          }
        })
    },
    getById(req,res){
        res.set('Content-Type', 'application/json');
        Statement.findById(req.params.id).then(results => {
            res.set('Content-Type', 'application/json');
            res.status(200).json({
                statement:results
            })
        }).catch(err => {
            res.status(404).json({
                statement:"NOT FOUND"
            })
        }) 
    },
    create(req,res){
        const {body} = req
        let x = Statement.createId(req.params.id).then( x=>{
            res.status(200).json({
                statement : x
            })
        }).catch(err=>console.log(err))
        
    },
    editById(req,res){
        const {body} = req
        const result = Statement.update(req.params.id,body).then(result => {
            res.set('Content-Type', 'application/json');
                if(result !== undefined){
                    res.status(201).json({
                        statement:result
                    })
                }
                else {
                    res.status(404).json("NOT FOUND")
                    
                }
          })
    },
    deleteById(req,res){
        const result = Statement.destroy(req.params.id).then(async results => {
            res.set('Content-Type', 'application/json');
            res.status(201).json({"result":"delete finish"})
          })
    }

}

export default StatementControllers