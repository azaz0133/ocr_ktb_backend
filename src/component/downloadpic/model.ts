import Model from '../model'
import {getConnection} from 'typeorm'
import classify from '../classify/controllers'
const Api = 'http://localhost:9001/api/'

const Up = {
  ...Model,
    async createId(filenamepic,url){
    
    const download = require('image-downloader')
       const options = {
        url,
        dest: `./src/asset/${filenamepic}.jpg`                  // Save to /path/to/dest/image.jpg
      }
      
      download.image(options)
        .then(({ filename, image }) => {
          console.log('File saved to', filename)
          classify.middleware({
            name:`${filenamepic}.jpg`,
            url
          })
        })
        .catch((err) => {
          console.error(err)
        })
    }
}

export default Up