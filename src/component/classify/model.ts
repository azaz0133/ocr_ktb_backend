import Axios from 'axios'

const Api = 'http://localhost:9001/api/'

const Classification = {
    async middleware(info){
        let  entity =""

        //to classify
        const config = {
            headers: {
              "Training-key": "5d7d6f9159314f4287d86842af20d6c1",
              "Content-Type": "application/json"
            }
    }
          const body = {
            Url:info.url
          }
          const url = "https://southcentralus.api.cognitive.microsoft.com/customvision/v1.1/Training/projects/d3c56ee4-75b5-4d90-9936-a9f901dc94ed/quicktest/url"
         await Axios.post(url,body,config)
               .then( res => {
                   entity = res.data.Predictions[0].Tag
                   console.log("type of image ",entity)
                   Axios.post(`${Api}${entity}/create/${info.name}`).then(res=>{
                       console.log("done please in database for sure")
                   })
               }).catch( err => console.log(err))
        // คิดวิธีในการส่งapi 
    }   
}




export default Classification