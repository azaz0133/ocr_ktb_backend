import {Entity, PrimaryGeneratedColumn, Column,CreateDateColumn,UpdateDateColumn} from "typeorm";

@Entity()
export default class ID_CARD {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    Id_cardnumber: string;

    @Column()
    birthDate: string;

    @Column()
    firstNameTh: string;

    @Column()
    lastNameTh: string;

    @Column()
    firstNameEn: string;

    @Column()
    lastNameEn: string;

    @Column()
    sex: string


    @Column('text')
    Address: string;
    @CreateDateColumn({type: "timestamp"})
    created_at: Date;

    @UpdateDateColumn({type: "timestamp"})
    updated_at: Date;


}